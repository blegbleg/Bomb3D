// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMAN3D_Bomb_generated_h
#error "Bomb.generated.h already included, missing '#pragma once' in Bomb.h"
#endif
#define BOMBERMAN3D_Bomb_generated_h

#define Bomb3D_Source_Bomberman3D_Bomb_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExplode) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->Explode(); \
		P_NATIVE_END; \
	}


#define Bomb3D_Source_Bomberman3D_Bomb_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExplode) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->Explode(); \
		P_NATIVE_END; \
	}


#define Bomb3D_Source_Bomberman3D_Bomb_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABomb(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_ABomb(); \
public: \
	DECLARE_CLASS(ABomb, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(ABomb) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_Bomb_h_16_INCLASS \
private: \
	static void StaticRegisterNativesABomb(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_ABomb(); \
public: \
	DECLARE_CLASS(ABomb, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(ABomb) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_Bomb_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABomb(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABomb) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABomb); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABomb); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABomb(ABomb&&); \
	NO_API ABomb(const ABomb&); \
public:


#define Bomb3D_Source_Bomberman3D_Bomb_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABomb(ABomb&&); \
	NO_API ABomb(const ABomb&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABomb); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABomb); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABomb)


#define Bomb3D_Source_Bomberman3D_Bomb_h_16_PRIVATE_PROPERTY_OFFSET
#define Bomb3D_Source_Bomberman3D_Bomb_h_13_PROLOG
#define Bomb3D_Source_Bomberman3D_Bomb_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_RPC_WRAPPERS \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_INCLASS \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bomb3D_Source_Bomberman3D_Bomb_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_INCLASS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_Bomb_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bomb3D_Source_Bomberman3D_Bomb_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
