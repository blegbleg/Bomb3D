// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMAN3D_PawnController_generated_h
#error "PawnController.generated.h already included, missing '#pragma once' in PawnController.h"
#endif
#define BOMBERMAN3D_PawnController_generated_h

#define Bomb3D_Source_Bomberman3D_PawnController_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execPlayerTwoRemoteBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerTwoRemoteBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerOneRemoteBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerOneRemoteBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerTwoSpeed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerTwoSpeed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerOneSpeed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerOneSpeed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerTwoBlast) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerTwoBlast(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerOneBlast) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerOneBlast(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerTwoBombs) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerTwoBombs(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerOneBombs) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerOneBombs(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnPlayerTwoBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnPlayerTwoBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnPlayerOneBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnPlayerOneBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerTwoMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Rightinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerTwoMoveRight(Z_Param_Rightinput); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerTwoMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Forwardinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerTwoMoveForward(Z_Param_Forwardinput); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerOneMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Rightinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerOneMoveRight(Z_Param_Rightinput); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerOneMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Forwardinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerOneMoveForward(Z_Param_Forwardinput); \
		P_NATIVE_END; \
	}


#define Bomb3D_Source_Bomberman3D_PawnController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execPlayerTwoRemoteBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerTwoRemoteBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerOneRemoteBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerOneRemoteBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerTwoSpeed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerTwoSpeed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerOneSpeed) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerOneSpeed(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerTwoBlast) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerTwoBlast(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerOneBlast) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerOneBlast(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerTwoBombs) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerTwoBombs(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execIncreasePlayerOneBombs) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->IncreasePlayerOneBombs(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnPlayerTwoBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnPlayerTwoBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execSpawnPlayerOneBomb) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->SpawnPlayerOneBomb(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerTwoMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Rightinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerTwoMoveRight(Z_Param_Rightinput); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerTwoMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Forwardinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerTwoMoveForward(Z_Param_Forwardinput); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerOneMoveRight) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Rightinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerOneMoveRight(Z_Param_Rightinput); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execPlayerOneMoveForward) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_Forwardinput); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		this->PlayerOneMoveForward(Z_Param_Forwardinput); \
		P_NATIVE_END; \
	}


#define Bomb3D_Source_Bomberman3D_PawnController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPawnController(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_APawnController(); \
public: \
	DECLARE_CLASS(APawnController, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(APawnController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_PawnController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAPawnController(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_APawnController(); \
public: \
	DECLARE_CLASS(APawnController, APawn, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(APawnController) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_PawnController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APawnController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APawnController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnController(APawnController&&); \
	NO_API APawnController(const APawnController&); \
public:


#define Bomb3D_Source_Bomberman3D_PawnController_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APawnController(APawnController&&); \
	NO_API APawnController(const APawnController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APawnController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APawnController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APawnController)


#define Bomb3D_Source_Bomberman3D_PawnController_h_16_PRIVATE_PROPERTY_OFFSET
#define Bomb3D_Source_Bomberman3D_PawnController_h_13_PROLOG
#define Bomb3D_Source_Bomberman3D_PawnController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_RPC_WRAPPERS \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_INCLASS \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bomb3D_Source_Bomberman3D_PawnController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_INCLASS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_PawnController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bomb3D_Source_Bomberman3D_PawnController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
