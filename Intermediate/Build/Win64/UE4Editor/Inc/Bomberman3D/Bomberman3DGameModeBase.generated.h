// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMAN3D_Bomberman3DGameModeBase_generated_h
#error "Bomberman3DGameModeBase.generated.h already included, missing '#pragma once' in Bomberman3DGameModeBase.h"
#endif
#define BOMBERMAN3D_Bomberman3DGameModeBase_generated_h

#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_RPC_WRAPPERS
#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABomberman3DGameModeBase(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_ABomberman3DGameModeBase(); \
public: \
	DECLARE_CLASS(ABomberman3DGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(ABomberman3DGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesABomberman3DGameModeBase(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_ABomberman3DGameModeBase(); \
public: \
	DECLARE_CLASS(ABomberman3DGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(ABomberman3DGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABomberman3DGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABomberman3DGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABomberman3DGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABomberman3DGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABomberman3DGameModeBase(ABomberman3DGameModeBase&&); \
	NO_API ABomberman3DGameModeBase(const ABomberman3DGameModeBase&); \
public:


#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABomberman3DGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABomberman3DGameModeBase(ABomberman3DGameModeBase&&); \
	NO_API ABomberman3DGameModeBase(const ABomberman3DGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABomberman3DGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABomberman3DGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABomberman3DGameModeBase)


#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_12_PROLOG
#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_RPC_WRAPPERS \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_INCLASS \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bomb3D_Source_Bomberman3D_Bomberman3DGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
