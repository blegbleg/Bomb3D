// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "PawnController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePawnController() {}
// Cross Module References
	BOMBERMAN3D_API UClass* Z_Construct_UClass_APawnController_NoRegister();
	BOMBERMAN3D_API UClass* Z_Construct_UClass_APawnController();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_Bomberman3D();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerOneBlast();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerOneBombs();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerOneSpeed();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerTwoBlast();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerTwoBombs();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerTwoSpeed();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_PlayerOneMoveForward();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_PlayerOneMoveRight();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_PlayerOneRemoteBomb();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_PlayerTwoMoveForward();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_PlayerTwoMoveRight();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_PlayerTwoRemoteBomb();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_SpawnPlayerOneBomb();
	BOMBERMAN3D_API UFunction* Z_Construct_UFunction_APawnController_SpawnPlayerTwoBomb();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter_NoRegister();
// End Cross Module References
	void APawnController::StaticRegisterNativesAPawnController()
	{
		UClass* Class = APawnController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IncreasePlayerOneBlast", (Native)&APawnController::execIncreasePlayerOneBlast },
			{ "IncreasePlayerOneBombs", (Native)&APawnController::execIncreasePlayerOneBombs },
			{ "IncreasePlayerOneSpeed", (Native)&APawnController::execIncreasePlayerOneSpeed },
			{ "IncreasePlayerTwoBlast", (Native)&APawnController::execIncreasePlayerTwoBlast },
			{ "IncreasePlayerTwoBombs", (Native)&APawnController::execIncreasePlayerTwoBombs },
			{ "IncreasePlayerTwoSpeed", (Native)&APawnController::execIncreasePlayerTwoSpeed },
			{ "PlayerOneMoveForward", (Native)&APawnController::execPlayerOneMoveForward },
			{ "PlayerOneMoveRight", (Native)&APawnController::execPlayerOneMoveRight },
			{ "PlayerOneRemoteBomb", (Native)&APawnController::execPlayerOneRemoteBomb },
			{ "PlayerTwoMoveForward", (Native)&APawnController::execPlayerTwoMoveForward },
			{ "PlayerTwoMoveRight", (Native)&APawnController::execPlayerTwoMoveRight },
			{ "PlayerTwoRemoteBomb", (Native)&APawnController::execPlayerTwoRemoteBomb },
			{ "SpawnPlayerOneBomb", (Native)&APawnController::execSpawnPlayerOneBomb },
			{ "SpawnPlayerTwoBomb", (Native)&APawnController::execSpawnPlayerTwoBomb },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerOneBlast()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "IncreasePlayerOneBlast", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerOneBombs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "IncreasePlayerOneBombs", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerOneSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "IncreasePlayerOneSpeed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerTwoBlast()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "IncreasePlayerTwoBlast", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerTwoBombs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "IncreasePlayerTwoBombs", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_IncreasePlayerTwoSpeed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "IncreasePlayerTwoSpeed", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_PlayerOneMoveForward()
	{
		struct PawnController_eventPlayerOneMoveForward_Parms
		{
			float Forwardinput;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Forwardinput = { UE4CodeGen_Private::EPropertyClass::Float, "Forwardinput", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PawnController_eventPlayerOneMoveForward_Parms, Forwardinput), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Forwardinput,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "PlayerOneMoveForward", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(PawnController_eventPlayerOneMoveForward_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_PlayerOneMoveRight()
	{
		struct PawnController_eventPlayerOneMoveRight_Parms
		{
			float Rightinput;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Rightinput = { UE4CodeGen_Private::EPropertyClass::Float, "Rightinput", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PawnController_eventPlayerOneMoveRight_Parms, Rightinput), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Rightinput,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "PlayerOneMoveRight", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(PawnController_eventPlayerOneMoveRight_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_PlayerOneRemoteBomb()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "PlayerOneRemoteBomb", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_PlayerTwoMoveForward()
	{
		struct PawnController_eventPlayerTwoMoveForward_Parms
		{
			float Forwardinput;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Forwardinput = { UE4CodeGen_Private::EPropertyClass::Float, "Forwardinput", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PawnController_eventPlayerTwoMoveForward_Parms, Forwardinput), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Forwardinput,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "PlayerTwoMoveForward", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(PawnController_eventPlayerTwoMoveForward_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_PlayerTwoMoveRight()
	{
		struct PawnController_eventPlayerTwoMoveRight_Parms
		{
			float Rightinput;
		};
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Rightinput = { UE4CodeGen_Private::EPropertyClass::Float, "Rightinput", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000080, 1, nullptr, STRUCT_OFFSET(PawnController_eventPlayerTwoMoveRight_Parms, Rightinput), METADATA_PARAMS(nullptr, 0) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_Rightinput,
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "PlayerTwoMoveRight", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, sizeof(PawnController_eventPlayerTwoMoveRight_Parms), PropPointers, ARRAY_COUNT(PropPointers), 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_PlayerTwoRemoteBomb()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "PlayerTwoRemoteBomb", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_SpawnPlayerOneBomb()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "SpawnPlayerOneBomb", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UFunction* Z_Construct_UFunction_APawnController_SpawnPlayerTwoBomb()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[] = {
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FFunctionParams FuncParams = { (UObject*(*)())Z_Construct_UClass_APawnController, "SpawnPlayerTwoBomb", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04020401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Function_MetaDataParams, ARRAY_COUNT(Function_MetaDataParams)) };
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_APawnController_NoRegister()
	{
		return APawnController::StaticClass();
	}
	UClass* Z_Construct_UClass_APawnController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			static UObject* (*const DependentSingletons[])() = {
				(UObject* (*)())Z_Construct_UClass_APawn,
				(UObject* (*)())Z_Construct_UPackage__Script_Bomberman3D,
			};
			static const FClassFunctionLinkInfo FuncInfo[] = {
				{ &Z_Construct_UFunction_APawnController_IncreasePlayerOneBlast, "IncreasePlayerOneBlast" }, // 2579236188
				{ &Z_Construct_UFunction_APawnController_IncreasePlayerOneBombs, "IncreasePlayerOneBombs" }, // 852106886
				{ &Z_Construct_UFunction_APawnController_IncreasePlayerOneSpeed, "IncreasePlayerOneSpeed" }, // 1152278572
				{ &Z_Construct_UFunction_APawnController_IncreasePlayerTwoBlast, "IncreasePlayerTwoBlast" }, // 1874491999
				{ &Z_Construct_UFunction_APawnController_IncreasePlayerTwoBombs, "IncreasePlayerTwoBombs" }, // 3301728645
				{ &Z_Construct_UFunction_APawnController_IncreasePlayerTwoSpeed, "IncreasePlayerTwoSpeed" }, // 2997362479
				{ &Z_Construct_UFunction_APawnController_PlayerOneMoveForward, "PlayerOneMoveForward" }, // 1103542001
				{ &Z_Construct_UFunction_APawnController_PlayerOneMoveRight, "PlayerOneMoveRight" }, // 4275283302
				{ &Z_Construct_UFunction_APawnController_PlayerOneRemoteBomb, "PlayerOneRemoteBomb" }, // 3648785570
				{ &Z_Construct_UFunction_APawnController_PlayerTwoMoveForward, "PlayerTwoMoveForward" }, // 3529641545
				{ &Z_Construct_UFunction_APawnController_PlayerTwoMoveRight, "PlayerTwoMoveRight" }, // 2617806213
				{ &Z_Construct_UFunction_APawnController_PlayerTwoRemoteBomb, "PlayerTwoRemoteBomb" }, // 3522259444
				{ &Z_Construct_UFunction_APawnController_SpawnPlayerOneBomb, "SpawnPlayerOneBomb" }, // 3218259197
				{ &Z_Construct_UFunction_APawnController_SpawnPlayerTwoBomb, "SpawnPlayerTwoBomb" }, // 1419176893
			};
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[] = {
				{ "HideCategories", "Navigation" },
				{ "IncludePath", "PawnController.h" },
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerTwo_MetaData[] = {
				{ "Category", "PawnController" },
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerTwo = { UE4CodeGen_Private::EPropertyClass::Object, "PlayerTwo", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000015, 1, nullptr, STRUCT_OFFSET(APawnController, PlayerTwo), Z_Construct_UClass_ACharacter_NoRegister, METADATA_PARAMS(NewProp_PlayerTwo_MetaData, ARRAY_COUNT(NewProp_PlayerTwo_MetaData)) };
#if WITH_METADATA
			static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerOne_MetaData[] = {
				{ "Category", "PawnController" },
				{ "ModuleRelativePath", "PawnController.h" },
			};
#endif
			static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerOne = { UE4CodeGen_Private::EPropertyClass::Object, "PlayerOne", RF_Public|RF_Transient|RF_MarkAsNative, 0x0010000000000015, 1, nullptr, STRUCT_OFFSET(APawnController, PlayerOne), Z_Construct_UClass_ACharacter_NoRegister, METADATA_PARAMS(NewProp_PlayerOne_MetaData, ARRAY_COUNT(NewProp_PlayerOne_MetaData)) };
			static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[] = {
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_PlayerTwo,
				(const UE4CodeGen_Private::FPropertyParamsBase*)&NewProp_PlayerOne,
			};
			static const FCppClassTypeInfoStatic StaticCppClassTypeInfo = {
				TCppClassTypeTraits<APawnController>::IsAbstract,
			};
			static const UE4CodeGen_Private::FClassParams ClassParams = {
				&APawnController::StaticClass,
				DependentSingletons, ARRAY_COUNT(DependentSingletons),
				0x00900080u,
				FuncInfo, ARRAY_COUNT(FuncInfo),
				PropPointers, ARRAY_COUNT(PropPointers),
				nullptr,
				&StaticCppClassTypeInfo,
				nullptr, 0,
				METADATA_PARAMS(Class_MetaDataParams, ARRAY_COUNT(Class_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUClass(OuterClass, ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APawnController, 2575475071);
	static FCompiledInDefer Z_CompiledInDefer_UClass_APawnController(Z_Construct_UClass_APawnController, &APawnController::StaticClass, TEXT("/Script/Bomberman3D"), TEXT("APawnController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APawnController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
