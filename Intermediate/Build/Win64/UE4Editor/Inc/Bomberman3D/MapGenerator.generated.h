// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMAN3D_MapGenerator_generated_h
#error "MapGenerator.generated.h already included, missing '#pragma once' in MapGenerator.h"
#endif
#define BOMBERMAN3D_MapGenerator_generated_h

#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_RPC_WRAPPERS
#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMapGenerator(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_AMapGenerator(); \
public: \
	DECLARE_CLASS(AMapGenerator, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(AMapGenerator) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAMapGenerator(); \
	friend BOMBERMAN3D_API class UClass* Z_Construct_UClass_AMapGenerator(); \
public: \
	DECLARE_CLASS(AMapGenerator, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/Bomberman3D"), NO_API) \
	DECLARE_SERIALIZER(AMapGenerator) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMapGenerator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMapGenerator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMapGenerator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapGenerator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMapGenerator(AMapGenerator&&); \
	NO_API AMapGenerator(const AMapGenerator&); \
public:


#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMapGenerator(AMapGenerator&&); \
	NO_API AMapGenerator(const AMapGenerator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMapGenerator); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapGenerator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMapGenerator)


#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_PRIVATE_PROPERTY_OFFSET
#define Bomb3D_Source_Bomberman3D_MapGenerator_h_11_PROLOG
#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_RPC_WRAPPERS \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_INCLASS \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Bomb3D_Source_Bomberman3D_MapGenerator_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_PRIVATE_PROPERTY_OFFSET \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_INCLASS_NO_PURE_DECLS \
	Bomb3D_Source_Bomberman3D_MapGenerator_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Bomb3D_Source_Bomberman3D_MapGenerator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
