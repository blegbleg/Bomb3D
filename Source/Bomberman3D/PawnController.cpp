// Fill out your copyright notice in the Description page of Project Settings.

#include "PawnController.h"
#include "GameFramework/Character.h"
#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Bomb.h"


// Sets default values
APawnController::APawnController()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UBlueprint> BombClass(TEXT("Blueprint'/Game/ThirdPersonBP/Blueprints/BombBP.BombBP'"));
	BombToSpawn = (UClass*)BombClass.Object->GeneratedClass;

}

// Called when the game starts or when spawned
void APawnController::BeginPlay()
{
	Super::BeginPlay();
	if (PlayerOne)
	{
		PlayerOneExist = true;
	}
	if (PlayerTwo)
	{
		PlayerTwoExist = true;
	}
	
}

// Called every frame
void APawnController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	PlayerTwoRemote -= DeltaTime;
	PlayerOneRemote -= DeltaTime;

}

// Called to bind functionality to input
void APawnController::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APawnController::PlayerOneMoveForward(float Forwardinput)
{
	if (PlayerOneExist)
	{
		PlayerOne->AddMovementInput(FVector(0, -1, 0), Forwardinput);
	}
}

void APawnController::PlayerOneMoveRight(float Rightinput)
{
	if (PlayerOneExist)
	{
		PlayerOne->AddMovementInput(FVector(1, 0, 0), Rightinput);
	}
}

void APawnController::PlayerTwoMoveForward(float Forwardinput)
{
	if (PlayerTwoExist)
	{
		PlayerTwo->AddMovementInput(FVector(0, -1, 0), Forwardinput);
	}
}

void APawnController::PlayerTwoMoveRight(float Rightinput)
{
	if (PlayerTwoExist)
	{
		PlayerTwo->AddMovementInput(FVector(1, 0, 0), Rightinput);
	}
}

void APawnController::SpawnPlayerOneBomb()
{
	if (!PlayerOneBombSpawned)
	{
		if (PlayerOneBombs > 0)
		{
			if (PlayerOneRemote > 0)
			{
				if (!PlayerOneBombSpawned)
				{
					LastBombSpawnedPlayerOne = SpawnPlayerBomb(PlayerOne);
					PlayerOneBombs -= 1;
					LastBombSpawnedPlayerOne->PlayerBomb = false;
					LastBombSpawnedPlayerOne->RemoteBomb = true;
					PlayerOneBombSpawned = true;
				}
				return;
			}
			LastBombSpawnedPlayerOne = SpawnPlayerBomb(PlayerOne);
			PlayerOneBombs -= 1;
			LastBombSpawnedPlayerOne->PlayerBomb = false;
		}
	}
	else
	{
		LastBombSpawnedPlayerOne->Explode();
		LastBombSpawnedPlayerOne->Destroy();
		PlayerOneBombSpawned = false;
	}
}

void APawnController::SpawnPlayerTwoBomb()
{
	if (!PlayerTwoBombSpawned)
	{
		if (PlayerTwoBombs > 0)
		{
			if (PlayerTwoRemote > 0)
			{
				LastBombSpawnedPlayerTwo = SpawnPlayerBomb(PlayerTwo);
				PlayerTwoBombs -= 1;
				LastBombSpawnedPlayerTwo->PlayerBomb = true;
				LastBombSpawnedPlayerTwo->RemoteBomb = true;
				PlayerTwoBombSpawned = true;
				return;
			}
			LastBombSpawnedPlayerTwo = SpawnPlayerBomb(PlayerTwo);
			PlayerTwoBombs -= 1;
			LastBombSpawnedPlayerTwo->PlayerBomb = true;
		}
	}
	else
	{
		LastBombSpawnedPlayerTwo->Explode();
		LastBombSpawnedPlayerTwo->Destroy();
		PlayerTwoBombSpawned = false;
	}
}

void APawnController::IncreasePlayerOneBombs()
{
	PlayerOneBombs += 1;
}

void APawnController::IncreasePlayerTwoBombs()
{
	PlayerTwoBombs += 1;
}

void APawnController::IncreasePlayerOneBlast()
{
	PlayerOneBlast += 1;
}

void APawnController::IncreasePlayerTwoBlast()
{
	PlayerTwoBlast += 1;
}

void APawnController::IncreasePlayerOneSpeed()
{
	PlayerOne->GetCharacterMovement()->MaxWalkSpeed += 100;
}

void APawnController::IncreasePlayerTwoSpeed()
{
	PlayerTwo->GetCharacterMovement()->MaxWalkSpeed += 100;
}

void APawnController::PlayerOneRemoteBomb()
{
	PlayerOneRemote = 10;
}

void APawnController::PlayerTwoRemoteBomb()
{
	PlayerTwoRemote = 10;
}

ABomb* APawnController::SpawnPlayerBomb(ACharacter* Player)
{
	FVector PlayerLocation = Player->GetActorLocation();
	float SpawnX = FMath::RoundToInt(PlayerLocation.X / 100) * 100;
	float SpawnY = FMath::RoundToInt(PlayerLocation.Y / 100) * 100;
	FVector SpawnLocation = FVector(SpawnX, SpawnY, 70);
	FActorSpawnParameters SpawnParam;
	SpawnParam.Instigator = this;
	ABomb* SpawnedBomb = GetWorld()->SpawnActor<ABomb>(BombToSpawn, SpawnLocation, FRotator(0, 0, 0), SpawnParam);
	return SpawnedBomb;
}

