// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"


class APawnController;
class UParticleSystem;

UCLASS()
class BOMBERMAN3D_API ABomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomb();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float ExplosionTime = 2;
	bool PlayerBomb = false;
	bool RemoteBomb = false;
	void ExplosionTrace(float TraceEnd,bool negative,bool direction);

	UFUNCTION(BlueprintCallable)
	void Explode();

private:
	APawnController* Control = nullptr;
	UParticleSystem* ExplosionParticle;
	float BombSize = 1;

	
	
};
