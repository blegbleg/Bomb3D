// Fill out your copyright notice in the Description page of Project Settings.

#include "Bomb.h"
#include "Engine/World.h"
#include "PawnController.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"


// Sets default values
ABomb::ABomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleClass(TEXT("ParticleSystem'/Game/StarterContent/Particles/P_Explosion.P_Explosion'"));
	ExplosionParticle = ParticleClass.Object;

}

// Called when the game starts or when spawned
void ABomb::BeginPlay()
{
	Super::BeginPlay();
	Control = Cast<APawnController>(Instigator);

}

// Called every frame
void ABomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!RemoteBomb)
	{
		if (ExplosionTime <= 0)
		{
			this->Destroy();
		}
		else
		{
			ExplosionTime -= DeltaTime;
		}
	}
}

void ABomb::ExplosionTrace(float TraceEnd,bool negative,bool direction)
{
	FHitResult TraceResult;
	FVector EndTrace;
	float i = 0;
	if (negative)
	{
		while(i < TraceEnd)
		{
			i += 1;
			FVector Loc;
			if (direction)
			{
				Loc = GetActorLocation() + FVector(i * -100, 0, 0);
				GetWorld()->LineTraceSingleByChannel(TraceResult, GetActorLocation(), Loc, ECC_Camera);
				if (TraceResult.GetComponent())
				{
					if (TraceResult.GetComponent()->GetCollisionObjectType() != ECC_WorldDynamic)
					{
						if (TraceResult.GetComponent()->GetCollisionObjectType() == ECC_WorldStatic)
						{
							TraceResult.GetActor()->Destroy();
							return;
						}
						TraceResult.GetActor()->Destroy();
					}
					else
					{
						return;
					}
				}
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticle, Loc);
			}
			else
			{
				Loc = GetActorLocation() + FVector(0, i * -100, 0);
				GetWorld()->LineTraceSingleByChannel(TraceResult, GetActorLocation(), Loc, ECC_Camera);
				if (TraceResult.GetComponent())
				{
					if (TraceResult.GetComponent()->GetCollisionObjectType() != ECC_WorldDynamic)
					{
						if (TraceResult.GetComponent()->GetCollisionObjectType() == ECC_WorldStatic)
						{
							TraceResult.GetActor()->Destroy();
							return;
						}
						TraceResult.GetActor()->Destroy();
					}
					else
					{
						return;
					}
				}
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticle, Loc);
			}
		}
	}
	else
	{
		while (i < TraceEnd)
		{
			i += 1;
			FVector Loc;
			if (direction)
			{
				Loc = GetActorLocation() + FVector(i * 100, 0, 0);
				GetWorld()->LineTraceSingleByChannel(TraceResult, GetActorLocation(), Loc, ECC_Camera);
				if (TraceResult.GetComponent())
				{
					if (TraceResult.GetComponent()->GetCollisionObjectType() != ECC_WorldDynamic)
					{
						if (TraceResult.GetComponent()->GetCollisionObjectType() == ECC_WorldStatic)
						{
							TraceResult.GetActor()->Destroy();
							return;
						}
							TraceResult.GetActor()->Destroy();
					}
					else
					{
						return;
					}
				}
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticle, Loc);
			}
			else
			{
				Loc = GetActorLocation() + FVector(0, i * 100, 0);
				GetWorld()->LineTraceSingleByChannel(TraceResult, GetActorLocation(), Loc, ECC_Camera);
				if (TraceResult.GetComponent())
				{
					if (TraceResult.GetComponent()->GetCollisionObjectType() != ECC_WorldDynamic)
					{
						if (TraceResult.GetComponent()->GetCollisionObjectType() == ECC_WorldStatic)
						{
							TraceResult.GetActor()->Destroy();
							return;
						}
							TraceResult.GetActor()->Destroy();
					}
					else
					{
						return;
					}
				}
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticle, Loc);
			}
		}
	}
}

void ABomb::Explode()
{
	if (Control)
	{
		if (!PlayerBomb)
		{
			Control->IncreasePlayerOneBombs();
		}
		else
		{
			Control->IncreasePlayerTwoBombs();
		}
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticle, GetActorLocation());
		if (!PlayerBomb)
		{
			BombSize = Control->PlayerOneBlast;
		}
		else
		{
			BombSize = Control->PlayerTwoBlast;
		}
		ExplosionTrace(BombSize, false, true);
		ExplosionTrace(BombSize, true, true);
		ExplosionTrace(BombSize, false, false);
		ExplosionTrace(BombSize, true, false);
	}
}

