// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PawnController.generated.h"

class ACharacter;
class ABomb;
class UParticleSystem;

UCLASS()
class BOMBERMAN3D_API APawnController : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APawnController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	ACharacter* PlayerOne;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ACharacter* PlayerTwo;

	UFUNCTION(BlueprintCallable)
		void PlayerOneMoveForward(float Forwardinput);
	UFUNCTION(BlueprintCallable)
		void PlayerOneMoveRight(float Rightinput);
	UFUNCTION(BlueprintCallable)
		void PlayerTwoMoveForward(float Forwardinput);
	UFUNCTION(BlueprintCallable)
		void PlayerTwoMoveRight(float Rightinput);
	UFUNCTION(BlueprintCallable)
		void SpawnPlayerOneBomb();
	UFUNCTION(BlueprintCallable)
		void SpawnPlayerTwoBomb();

	UFUNCTION(BlueprintCallable)
	void IncreasePlayerOneBombs();
	UFUNCTION(BlueprintCallable)
	void IncreasePlayerTwoBombs();
	UFUNCTION(BlueprintCallable)
	void IncreasePlayerOneBlast();
	UFUNCTION(BlueprintCallable)
	void IncreasePlayerTwoBlast();
	UFUNCTION(BlueprintCallable)
	void IncreasePlayerOneSpeed();
	UFUNCTION(BlueprintCallable)
	void IncreasePlayerTwoSpeed();
	UFUNCTION(BlueprintCallable)
	void PlayerOneRemoteBomb();
	UFUNCTION(BlueprintCallable)
	void PlayerTwoRemoteBomb();

	int PlayerOneBlast = 1;
	int PlayerTwoBlast = 1;


private:
	bool PlayerOneExist = false;
	bool PlayerTwoExist = false;
	int PlayerOneBombs = 1;
	int PlayerTwoBombs = 1;
	float PlayerOneRemote = 0;
	bool PlayerOneBombSpawned = false;
	bool PlayerTwoBombSpawned = false;
	float PlayerTwoRemote = 0;
	ABomb* LastBombSpawnedPlayerOne = nullptr;
	ABomb* LastBombSpawnedPlayerTwo = nullptr;
	TSubclassOf<class ABomb> BombToSpawn;
	ABomb* SpawnPlayerBomb(ACharacter* Player);
	UParticleSystem* ExplosionParticle;

	
	
};
