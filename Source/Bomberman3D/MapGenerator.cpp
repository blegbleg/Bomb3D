// Fill out your copyright notice in the Description page of Project Settings.

#include "MapGenerator.h"
#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "PawnController.h"


// Sets default values
AMapGenerator::AMapGenerator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UBlueprint> WallClass(TEXT("Blueprint'/Game/ThirdPersonBP/Blueprints/Wall.Wall'"));
	WallToSpawn = (UClass*)WallClass.Object->GeneratedClass;

}

// Called when the game starts or when spawned
void AMapGenerator::BeginPlay()
{
	Super::BeginPlay();
	while(Spawning)
	{
		if (!FMath::IsNearlyEqual(GetActorLocation().X, MapXStart, 150) || !FMath::IsNearlyEqual(GetActorLocation().Y, MapYStart, 150))
		{
			if (!FMath::IsNearlyEqual(GetActorLocation().X, MapXEnd, 150) || !FMath::IsNearlyEqual(GetActorLocation().Y, MapYEnd, 150))
			{
				if (GetActorLocation().Y != MapYEnd)
				{
					SpawnWall();
					SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y + SpawnJump, 70));
				}
				else
				{
					SpawnWall();
					SetActorLocation(FVector(GetActorLocation().X + 100, MapYStart, 70));
					if (SpawnJump == 100)
					{
						SpawnJump = 200;
					}
					else
					{
						SpawnJump = 100;
					}
				}
			}
			else if (GetActorLocation().X != MapXEnd)
			{
				SetActorLocation(FVector(GetActorLocation().X + 100, MapYStart, 70));
				SpawnJump = 100;
			}
			else
			{
				Spawning = false;
			}
		}
		else
		{
			SetActorLocation(FVector(GetActorLocation().X, GetActorLocation().Y + SpawnJump, 70));
		}
	}

}

// Called every frame
void AMapGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMapGenerator::SpawnWall()
{
	if (FMath::RandRange(0, 100) < ChangeToSpawn)
	{
		FActorSpawnParameters SpawnParam;
		SpawnParam.Instigator = PawnController;
		AActor* SpawnedWall = GetWorld()->SpawnActor<AActor>(WallToSpawn, this->GetActorTransform(), SpawnParam);
	}
}

