// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MapGenerator.generated.h"

class APawnController;

UCLASS()
class BOMBERMAN3D_API AMapGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMapGenerator();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	float ChangeToSpawn = 35;
	UPROPERTY(EditAnywhere)
	APawnController* PawnController;

	float MapYStart = -900;
	float MapXStart = -900;
	float MapYEnd = 900;
	float MapXEnd = 1900;

	void SpawnWall();


private:

	bool Spawning = true;
	float SpawnJump = 100;
	TSubclassOf<class AActor> WallToSpawn;

	
	
};
